# 07 | 线性结构

## 大O复杂度表示法简介

什么样的程序才是好的程序？好的程序设计无外乎两点，“快”和“省”。“快”指程序执行速度快，高效，“省”指占用更小的内存空间。这两点其实就对应“时间复杂度”和“空间复杂度”的问题。

“大O表示法”表示程序的执行时间或占用空间随数据规模的增长趋势。大O表示法就是将代码的所有步骤转换为关于数据规模n的公式项，然后排除不会对问题的整体复杂度产生较大影响的低阶系数项和常数项。

### 时间复杂度

时间复杂度，又称"渐进式时间复杂度"，表示代码执行时间与数据规模之间的增长关系。用公式来表达就是T(n) = O(f(n))

- n：数据规模，通俗点说就是函数中的那个变量n
- f(n)：代码总的执行次数和数据规模的关系
- T(n)：代码的执行时间(并不是代码实际的执行时间，这里表示代码执行时间和数据规模之间的关系)

关于时间复杂度分析，我们需要关注以下几点：

- 只关注循环执行次数最多的那段代码
- 加法法则：总复杂度等于量级最大的那段代码的复杂度
    - 常量阶(O(1))：代码片段执行时间不随数据规模n的增加而增加，即使执行次数非常大，只要与数据规模无关，都算作常量阶。
    - 按量级递增排序：常量阶O(1)) < 对数阶O(logn) <  线性阶O(n) < 线性对数阶O(nlog(n)) < 平方阶O(n ^ 2)...立方阶O(n ^ 3)...k方阶 < 指数阶O(2 ^ n}) < 阶乘阶O(n!) 。其中，O(2 ^ n})和O(n!)为非多项式量级，其他的为多项式量级。我们把时间复杂度为非多项式量级的算法问题叫作NP（Non-Deterministic Polynomial，非确定多项式）问题。
    - 乘法法则：嵌套代码复杂度等于内外代码复杂度的乘积

### 空间复杂度

空间复杂度，也称渐进空间复杂度，表示代码存储空间与数据规模之间的增长关系。相对时间复杂度，空间复杂度比较简单，常用的一般是O(1)或者O(n)，O(n ^ 2)也比较少见，其它更高阶的复杂度基本可以忽略。

## 常用线性结构及特点

- 数组
    - 可以快速的随机访问
    - 添加/删除元素成本高，平均O(n)的时间复杂度。
- 链表
    - 不支持随机访问
    - 添加元素成本低，O(1)的时间复杂度。
    - 如果是双向链表，删除也是O(1)的时间复杂度
- 堆栈
    - 元素先进后出（First In Last Out），可以基于数组实现。
- 队列
    - 元素先进先出（First In First Out），从效率角度出发，可以基于链表实现。

### 实现简单的堆栈

需要实现以下方法：

- 构造函数：指定堆栈大小
- isEmpty：判断堆栈是否为空
- isFull：判断堆栈是否填满
- push：元素入栈
- pop：弹出栈顶元素
- peek：查询栈顶元素

```java
// MyStack.java
public class MyStack<T> {
    T[] arr;
    int capacity, size;

    MyStack(int capacity) {
        this.size = 0;
        this.capacity = capacity;
        this.arr =  (T[])new Object[this.capacity];
    }

    public boolean isEmpty() { return size == 0; }

    public boolean isFull() { return size == capacity; }

    public void push(T item) throws Exception {
        if (!isFull()) {
            arr[size] = item;
            ++size;
        } else {
            throw new Exception("Stack is full");
        }
    }

    public T pop() throws Exception {
        if (!isEmpty()) {
            T ret = arr[size - 1];
            --size;
            return ret;
        } else {
            throw new Exception("Stack is empty");
        }
    }

    public T peek() throws Exception {
        if (!isEmpty()) {
            return arr[size - 1];
        } else {
            throw new Exception("Stack is empty");
        }
    }
}

MyStack<String> stack = new MyStack<>(5);
stack.push("ABC");
stack.push("BCD");
stack.push("CDE");
System.out.println(stack.peek());
while (!stack.isEmpty()) {
    String s = stack.pop();
    System.out.println(s);
}

// 实现链表

需要实现以下方法：

- getHead：获取头部元素
- add：追加节点
- remove：删除节点

```java
// Node.java
public class Node<T> {
    public T val;
    public Node<T> next;

    Node(T val) {
        this.val = val;
        next = null;
    }
}

// LList.java
public class LList<T> {
    Node<T> head, tail;

    LList() {
        head = null;
        tail = null;
    }

    boolean isEmpty() { return head == null; }

    void add(Node<T> node) {
        if (head == null) {
            head = node;
        } else {
            tail.next = node;
        }
        tail = node;
    }

    Node<T> getHead() { return head; }

    void remove(Node<T> node) { // 一定要回去自己实现一遍！！！
        Node<T> new_head = head;
        Node<T> new_tail = tail;
        if (new_head != null) {
            Node<T> prev = null;
            Node<T> n = new_head;
            while (n != null) {
                if (n.val == node.val) {
                    if (prev != null) {
                        prev.next = n.next;
                    } else {
                        new_head = n.next;
                    }
                    if (n == tail) {
                        new_tail = prev;
                    }
                    break;
                } else {
                    prev = n;
                    n = n.next;
                }
            }
        }
        head = new_head;
        tail = new_tail;
    }
}

Node<String> n1 = new Node<>("ABC");
Node<String> n2 = new Node<>("BCD");
Node<String> n3 = new Node<>("CDE");
Node<String> n4 = new Node<>("DEF");
Node<String> n5 = new Node<>("EFG");
LList<String> lList = new LList<>();
lList.add(n1);
lList.add(n2);
lList.add(n3);
lList.add(n4);
lList.add(n5);
lList.remove(n1);
lList.add(new Node<String>("FGH"));
Node<String> node = lList.getHead();
while (node != null) {
    System.out.println(node.val);
    node = node.next;
}
```

### 实现队列（基于之前实现的链表）

需要实现以下方法：

- push：元素入列
- pop：元素出列

```java
// Queue.java
public class Queue<T> {
    LList<T> lList;

    Queue() { lList = new LList<>(); }

    boolean isEmpty() { return  lList.isEmpty(); }

    void push(T val) {
        lList.add(new Node<>(val));
    }

    T pop() throws Exception {
        if (isEmpty()) {
            throw new Exception("Queue is empty");
        }
        Node<T> node = lList.getHead();
        lList.remove(node);
        return node.val;
    }
}

Queue<String> queue = new Queue<>();
queue.push("ABC");
queue.push("BCD");
queue.push("CDE");
while (!queue.isEmpty()) {
    String s = queue.pop();
    System.out.println(s);
}
```

## 实战练习

### [翻转一个链表](https://www.lintcode.com/problem/reverse-linked-list/)

- 样例1：
    - 输入: 1->2->3->null
    - 输出: 3->2->1->null
- 样例2
    - 输入: 1->2->3->4->null
    - 输出: 4->3->2->1->null

这道题目属于非常经典的题目，但是在实际面视中，还是有一半同学不能很好的解答。思路如下：

- 如果旧链表的head（记为<b>H</b>）不为null
    - 旧链表新的head为原来<b>H</b>的next
    - 如果新链表不存在，使用<b>H</b>作为新链表的head和tail。
    - 如果新链表已存在，H的next指向新链表的head，设置<b>H</b>为新链表的head。
- 如果旧链表的head为null，返回新链表的head。

```java
public class Solution {
    /**
     * @param head: n
     * @return: The new head of reversed linked list.
     */
    public ListNode reverse(ListNode head) {
        // write your code here
        ListNode new_head = null, new_tail = null;
        while (head != null) {
            if (new_head == null) {
                new_head = head;
                new_tail = new_head;
                head = head.next;
                new_tail.next = null;
            }
            else {
                ListNode _head = head.next;
                head.next = new_head;
                new_head = head;
                head = _head;

            }
        }
        return new_head;
    }
}
```

### [搜索二维矩阵](https://www.lintcode.com/problem/search-a-2d-matrix/)

这题是数组二分法查找的扩展。

这个矩阵具有以下特性：

    - 每行中的整数从左到右是排序的。
    - 每行的第一个数大于上一行的最后一个整数。

- 样例1:

```
输入: [[5]],2
输出: false
样例解释: 没有包含，返回false。
```

- 样例 2:

```
输入:  

    [
        [1, 3, 5, 7],
        [10, 11, 16, 20],
        [23, 30, 34, 50]
    ],3
	输出: true
	样例解释: 包含则返回true。
```

因为数组不管是每行还是每列都是有序的，我们可以先通过二分法定位数据可能出现在某行，然后在该行使用二分法判断元素是否存在。第一次二分法满足的条件是如果行数大于1，该行第一个元素小于目标元素，但是下一行的第一个元素必须大于目标元素。

```java
public class Solution {
    /**
     * @param matrix: matrix, a list of lists of integers
     * @param target: An integer
     * @return: a boolean, indicate whether matrix contains target
     */
    public boolean searchMatrix(int[][] matrix, int target) {
        // write your code here
        if (matrix.length > 0) {
            int rows = matrix.length;
            int cols = matrix[0].length;
            if ((target >= matrix[0][0]) && (target <= matrix[rows -1][cols - 1])) {
                int start_row = 0;
                int end_row = rows - 1;
                int row = 0;
                while (start_row <= end_row) { // 目标是m[r][0] <= target并且m[r + 1][0] > target
                    row = (start_row + end_row) / 2;
                    if (matrix[row][0] < target) {
                        start_row = row + 1;
                    }
                    else if (matrix[row][0] > target) {
                        end_row = row - 1;
                    }
                    else {
                        break;
                    }
                }
                if ((row > 0) && (matrix[row][0] > target)) { // 必不可少！因为前面不是判断相等。
                    --row;
                }
                if ((matrix[row][0] <= target) && (matrix[row][cols -1] >= target)) {
                    int start_col = 0;
                    int end_col = cols - 1;
                    int col = 0;
                    while (start_col <= end_col) {
                        col = (start_col + end_col) / 2;
                        if (matrix[row][col] < target) {
                            start_col = col + 1;
                        } else if (matrix[row][col] > target) {
                            end_col = col - 1;
                        } else {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
```

### [有效的括号序列](https://www.lintcode.com/problem/valid-parentheses/)

关于堆栈的应用

给定一个字符串所表示的括号序列，包含以下字符： '(', ')', '{', '}', '[' and ']'， 判定是否是有效的括号序列。
括号必须依照 "()" 顺序表示， "()[]{}" 是有效的括号，但 "([)]" 则是无效的括号。

遇到左括号入栈，遇到右括号对比栈顶元素，如果括号能够匹配就把栈顶元素弹出。最后如果堆栈为空则是有效括号序列，否则代表括号不匹配，不是有效序列。

```java
public class Solution {
    /**
     * @param s: A string
     * @return: whether the string is a valid parentheses
     */
    public boolean isValidParentheses(String s) {
        // write your code here
        Stack<Character> stack = new Stack<>();
        for (int i = 0; i < s.length(); ++i) {
            char ch = s.charAt(i);
            if (('(' == ch) || ('[' == ch) || ('{' == ch)) {
                stack.push(ch);
            }
            else if (')' == ch) {
                if ((!stack.isEmpty()) && (stack.peek() == '(')) {
                    stack.pop();
                }
                else {
                    return false;
                }
            }
            else if (']' == ch) {
                if ((!stack.isEmpty()) && (stack.peek() == '[')) {
                    stack.pop();
                }
                else {
                    return false;
                }
            }
            else { // }
                if ((!stack.isEmpty()) && (stack.peek() == '{')) {
                    stack.pop();
                }
                else {
                    return false;
                }
            }
        }
        return stack.isEmpty();
    }
}
```

### [数据流滑动窗口平均值](https://www.lintcode.com/problem/moving-average-from-data-stream/description)

可以用队列，也可以用滑动窗口解决。

给出一串整数流和窗口大小，计算滑动窗口中所有整数的平均值。

样例

- MovingAverage m = new MovingAverage(3);
- m.next(1) = 1 // 返回 1.00000
- m.next(10) = (1 + 10) / 2 // 返回 5.50000
- m.next(3) = (1 + 10 + 3) / 3 // 返回 4.66667
- m.next(5) = (10 + 3 + 5) / 3 // 返回 6.00000

思路：

- 队列：维护一个队列，如果队列长度小于窗口，计算总和并除以队列长度。如果队列已满，移除头部元素，总和减去该元素，尾部添加新元素，总和加上该新元素，返回综合除以队列长度。

```java
public class MovingAverage {
    int size = 0;
    double sum = 0;
    Queue<Integer> queue;
    /*
    * @param size: An integer
    */public MovingAverage(int size) {
        // do intialization if necessary
        this.size = size;
        this.queue = new LinkedList<>();
    }

    /*
     * @param val: An integer
     * @return:  
     */
    public double next(int val) {
        // write your code here
        if (queue.size() == size) {
            sum -= queue.poll();
        }
        sum += val;
        queue.add(val);
        return sum / (double)queue.size();
    }
}
```
- 滑动窗口：维护一个大小为N的数组，第0个元素放在位置0，第1个元素放在位置1，第N - 1个元素放在N - 1，第N个元素重新放置到位置0，可以得出结论，第k个元素摆放的位置是k % N，如果k >= N，则之前该位置上的元素被覆盖。计算平均值的方法与之前类似。

```java
public class MovingAverage {
    int size;
    int[] queue;
    double total;
    int pos, count;
    
    /*
    * @param size: An integer
    */public MovingAverage(int size) {
        // do intialization if necessary
        this.size = size;
        queue = new int[size];
        total = 0.0f;
        pos = 0;
        count = 0; // 窗口是否填满
    }

    /*
     * @param val: An integer
     * @return:  
     */
    public double next(int val) {
        // write your code here
        total += val;
        if (count == size) {
            pos %= size;
            total -= queue[pos];
            queue[pos] = val;
            pos += 1;
            
        } else {
            queue[pos] = val;
            pos += 1;
            count += 1;
        }
        return total / (double)count;
    }
}
```

### [最大子数组](https://www.lintcode.com/problem/maximum-subarray/)

这题是早年各大厂面视最爱，经典的贪心算法。

给定一个整数数组，找到一个具有最大和的子数组，返回其最大和。

样例1:

```
输入：[−2,2,−3,4,−1,2,1,−5,3]
输出：6
解释：符合要求的子数组为[4,−1,2,1]，其最大和为 6。
```

样例2:

```
输入：[1,2,3,4]
输出：10
解释：符合要求的子数组为[1,2,3,4]，其最大和为 10。
```

思路：

- 暴力2重循环肯定可以解决问题，但是时间复杂度是O(n ^ 2)。
- 如何把复杂度降为一重循环：
    - 从第一个元素开始，累加求和。
    - 如果当前和大于0：更新最大值。
    - 如果当前和小于0，前面都扔掉不用。往后从第一个大于0的元素开始。<b>想一想</b>，为什么整个这段可以不要？

```java
public class Solution {
    /**
     * @param nums: A list of integers
     * @return: A integer indicate the sum of max subarray
     */
    public int maxSubArray(int[] nums) {
        // write your code here
        int ret = Integer.MIN_VALUE;
        int i = 0, sum = 0;
        while (i < nums.length) {
            sum += nums[i];
            if (sum > ret) {
                ret = sum;
            }
            if (sum < 0) {
                sum = 0;
            }
            ++i;
        }
        return ret;
    }
}
```

### 思考题：双队列实现栈

队列是先进先出，而堆栈是先进后厨。所以思路如下：

- 两个队列分别命名为A和B，两个队列都为空。
- 执行push操作
    - 如果A为空，直接push进A。
    - 如果A不为空，先将A的内容弹出逐个push进入B，将元素push进入A，再将B的内容弹出逐个push进入A。
- 执行pop操作
    - 从A中pop元素直到空为止

## 课程推荐

[![大数据工程师集训营](../ad_img/bigdata.jpg)](http://www.julyedu.com/Weekend/bigdata)

课程链接：[http://www.julyedu.com/Weekend/bigdata](http://www.julyedu.com/Weekend/bigdata)

原价<b>9499</b>面向“Java与数据结构”班学员特别优惠价格<b>6499</b>!!!报名点击报名页面的“<b>课程咨询</b>”按钮信息修改价格。

### 课程简介

- 本大数据集训营从Hadoop基础讲解，贯穿数据采集、传输、存储、计算、展示等各个环节，着重讲解企业中如何使用spark、MapReduce、hive、flume、sqoop等各个组件，并附有经典企业案例讲解，案例均来自一线互联网工业项目。
- 2019年11月下旬时，我们讲师团队再次加强，堪称大厂豪华级大数据专家讲师团队，且根据最近的大数据人才需求，加入elasticsearch和数据仓库模型等内容。
- 2020年3月下旬时，全面优化大纲，比如新增以下三大实战项目，且标准化项目流程：
  - 设计与搭建基于Hive、Presto的数据仓库与OLAP分析引擎
  - Flink企业实战——直播、短视频APP用户行为分析
  - ELK+Spark实现一个错误日志监控(搜索、分析、报警)平台

### 培养目标

从<b>零</b>开始，由Hadoop入门，打造大数据开发工程师之路。