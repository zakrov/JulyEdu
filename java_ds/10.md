# 10 | 递归

## 递归的定义

程序调用自身的编程技巧称为递归（ recursion）。递归做为一种算法在程序设计语言中广泛应用。 一个过程或函数在其定义或说明中有直接或间接调用自身的一种方法，它通常把一个大型复杂的问题层层转化为一个与原问题相似的规模较小的问题来求解，递归策略只需少量的程序就可描述出解题过程所需要的多次重复计算，大大地减少了程序的代码量。

简单说，递归就是在运行的过程中调用自己。构成递归需具备的条件：

- 子问题须与原始问题为同样的事，且更为简单；
- <b>不能无限制地调用本身</b>，须有个出口，化简为非递归状况处理。

经典的递归问题：

- 阶乘：f(n) = n * f(n - 1), f(1) = 1
- 斐波那契数列：f(n) = f(n - 1) + f(n - 2), f(0) = f(1) = 1

递归算法一般用于解决三类问题：

1. 数据的定义是按递归定义的。（Fibonacci函数）
2. 问题解法按递归算法实现。这类问题虽则本身没有明显的递归结构，但用递归求解比迭代求解更简单，如Hanoi问题。
3. 数据的结构形式是按递归定义的。如二叉树、广义表等，由于结构本身固有的递归特性，则它们的操作可递归地描述。

### 用递归实现字符串反转

用前面构成递归需具备的条件来思考一下，得出如下结论：

1. 如果字符串长度为n，交换第0和第n - 1个字符，然后递归反转子传1到n - 2。（更简单但是相同的问题）
2. 如果子串长度小于等于1，直接结束返回。（出口）

试着自己实现代码

### 递归典型问题： 梵塔问题（汉诺塔问题）

已知有三根针分别用A, B, C表示，在A中从上到下依次放n个从小到大的盘子，现要求把所有的盘子从A针全部移到B针，移动规则是：可以使用C临时存放盘子，每次只能移动一块盘子，而且每根针上不能出现大盘压小盘，找出移动次数最小的方案。

实现步骤如下：初始问题可以描述成N个盘子从A借助C移动到B。如果只有1个盘子直接移动，否则按如下步骤不断操作。

1. 把A针上的N - 1个盘子借助B移动到C
  1. 把A针上的N - 2个盘子借助C移动到B
    1. ...
    2. ...
    3. ...
  2. 把A针上的第N - 1个盘子移动到C
  3. 把B针上的N - 2个盘子借助A移动到C
2. 把A针上的第N个盘子移动到B
3. 把C针上的N - 1个盘子借助A移动到B

第1/3步就把N个盘子的问题变成了N - 1个盘子的问题。然后不断降低，最终变成1个盘子的问题。

实现代码

```java
private void hanoi(int n, char from, char to, char by) {
    if (n > 0) {
        hanoi(n - 1, from, by, to);
        System.out.println("move " + n + " from " + from + " to " + to);
        hanoi(n - 1, by, to, from);
    }
}
```

## 实战练习

### [二叉树的前序遍历](https://www.lintcode.com/problem/binary-tree-preorder-traversal/)

给出一棵二叉树，返回其节点值的前序遍历。

样例 1:

```text
输入：{1,2,3}
输出：[1,2,3]
解释：
   1
  / \
 2   3
它将被序列化为{1,2,3}
```

样例 2:

```text
输入：{1,#,2,3}
输出：[1,2,3]
解释：
1
 \
  2
 /
3
它将被序列化为{1,#,2,3}
```

思路：

1. 输出根节点
2. 遍历左子树（更简单但是相同的问题）
3. 遍历右子树（更简单但是相同的问题）
4. 如果节点为null直接返回。（出口）

实现代码如下：

```java
public class Solution {
    /**
     * @param root: A Tree
     * @return: Preorder in ArrayList which contains node values.
     */
    public List<Integer> preorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        preorderTraversal(root, ret);
        return ret;
    }
    
    public void preorderTraversal(TreeNode root, List<Integer> ret) {
        // write your code here
        if (root != null) {
            ret.add(root.val);
            preorderTraversal(root.left, ret);
            preorderTraversal(root.right, ret);
        }
    }
}
```

在这个基础上自己实现中/后序遍历的递归版本。

### [克隆二叉树](https://www.lintcode.com/problem/clone-binary-tree/)

深度复制一个二叉树。给定一个二叉树，返回一个他的 克隆品 。

思路：

1. 复制根节点，如果为空直接返回（出口）
2. 复制左子树（更简单但是相同的问题）
3. 复制右子树（更简单但是相同的问题）

实现代码如下：

```java
public class Solution {
    /**
     * @param root: The root of binary tree
     * @return: root of new tree
     */
    public TreeNode cloneTree(TreeNode root) {
        // write your code here
        if (root != null) {
            TreeNode new_root = new TreeNode(root.val);
            if (root.left != null) {
                new_root.left = cloneTree(root.left);
            }
            if (root.right != null) {
                new_root.right = cloneTree(root.right);
            }
            return new_root;
        }
        return null;
    }
}
```

### [中序遍历和后序遍历树构造二叉树](https://www.lintcode.com/problem/construct-binary-tree-from-inorder-and-postorder-traversal/)

根据中序遍历和后序遍历树构造二叉树

<b>你可以假设树中不存在相同数值的节点</b>（这个假设非常关键！！！）

样例 1:

```text
输入：[],[]
输出：{}
解释：
二叉树为空
```

样例 2:

```text
输入：[1,2,3],[1,3,2]
输出：{2,1,3}
解释：
二叉树如下
  2
 / \
1   3
```

这题做为中等难度，初看很容易把人吓着...回想一下前中后序遍历的定义，前中后实际是根节点的位置。前序：根-左-右，中序：左-根-右，后序：左-右-根。所以我们根据后序遍历的结果，可以知道根节点的值。根据根节点的值，可以在中序遍历的结果里区分出左右子树（也是中序遍历过的）。根据上一步可以得到左右子树节点的数量，那么在后序遍历的结果里我们也可以分出左右子树（后续遍历过的），然后左右子树分别构造。

实现代码如下：

```java
public class Solution {
    /**
     * @param inorder: A list of integers that inorder traversal of a tree
     * @param postorder: A list of integers that postorder traversal of a tree
     * @return: Root of a tree
     */
    public TreeNode buildTree(int[] inorder, int[] postorder) {
        // write your code here
        return _buildTree(inorder, 0, inorder.length, postorder, 0, postorder.length);
    }
    
    private TreeNode _buildTree(int[] inorder, int istart, int iend, int[] postorder, int pstart, int pend) {
        if (istart >= iend) {
            return null;
        }
        int i = istart;
        while (i < iend) { // 找到中序的根节点
            if (inorder[i] == postorder[pend - 1]) {
                break;
            }
            ++i;
        }
        TreeNode root = new TreeNode(inorder[i]);
        int llen = i - istart; // 左子树节点数量
        root.left = _buildTree(inorder, istart, i, postorder, pstart, pstart + llen);
        root.right = _buildTree(inorder, i + 1, iend, postorder, pstart + llen, pend - 1);
        return root;
    }
}
```

### [全排列](https://www.lintcode.com/problem/permutations/)

给定一个数字列表，返回其所有可能的排列。假设没有重复数字，不然就是另一个问题，可以试着挑战一下。

样例 1：

```text
输入：[1]
输出：
[
  [1]
]
```

样例 2：

```text
输入：[1,2,3]
输出：
[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
```

思路：排列组合问题首选递归！非递归可以解决，但是代码不是那么好理解。

以输入[1, 2, 3]为例：

1. 第一个元素为1，输出[2, 3]的全排列，前面加1，得到[1, 2, 3]和[1, 3, 2]。
2. 第一个元素为2，输出[1, 3]的全排列，前面加1，得到[2, 1, 3]和[2, 3, 1]。
3. 第一个元素为3，输出[1, 2]的全排列，前面加1，得到[3, 1, 2]和[3, 2, 1]。

具体实现需要一点技巧：1）通过与第1个元素进行交换，把2，3等放到开始位置；2）在处理到最后一个元素的时候，输出全部排列。

实现代码如下：

```java
public class Solution {
    /*
     * @param nums: A list of integers.
     * @return: A list of permutations.
     */
    public List<List<Integer>> permute(int[] nums) {
        // write your code here
        List<List<Integer>> ret = new ArrayList<>();
        if (nums.length > 0) {
            _permute(nums, 0, ret);
        } else {
            ret.add(new ArrayList<>());
        }
        return ret;
    }
    
    protected void _permute(int[] nums, int start, List<List<Integer>> ret) {
        if (start == nums.length) {
            List<Integer> li = new ArrayList<>();
            for (int i : nums) {
                li.add(i);
            }
            ret.add(li);
        } else {
            for (int i = start; i < nums.length; ++i) {
                swap(nums, start, i);
                _permute(nums, start + 1, ret);
                swap(nums, i, start);
            }
        }
    }
    
    protected void swap(int[] nums, int i, int j) {
        int tmp = nums[i];
        nums[i] = nums[j];
        nums[j] = tmp;
    }
}
```

### [子集](https://www.lintcode.com/problem/subsets/description)

给定一个含不同整数的集合，返回其所有的子集。

```text
子集中的元素排列必须是非降序的，解集必须不包含重复的子集。
```

样例 1：

```text
输入：[0]
输出：
[
  [],
  [0]
]
```

样例 2：

```text
输入：[1,2,3]
输出：
[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]
```

思路：

- 答案要求排练是有序的，啥都不说，先排个序。
- 空集一定是答案的一部分
- 开始递归：
    - 取出第一个元素，生成后面所有元素的排列（更简单但是相同的问题）。再把1和以上结果组合放入最终的返回集合内。
    - 如果只有一个元素，直接输出。（出口）

实现代码如下：

```java
public class Solution {
    /**
     * @param nums: A set of numbers
     * @return: A list of lists
     */
    public List<List<Integer>> subsets(int[] nums) {
        // write your code here
        List<List<Integer>> ret = new ArrayList<>();
        Arrays.sort(nums);
        if (nums.length == 0) {
            ret.add(new ArrayList<>());
        } else {
            for (int i = 0; i <= nums.length; ++i) {
                List<List<Integer>> ss = _subsets(nums, 0, i); // 从0开始，包含i个元素的子集。
                if (ss != null) {
                    for (List<Integer> subset : ss) {
                        ret.add(subset);
                    }
                }
            }
        }
        return ret;
    }
    
    protected List<List<Integer>> _subsets(int[] nums, int start, int n) {
        List<List<Integer>> ret = new ArrayList<>();
        int len = nums.length - start; // 可以提供的长度
        // 检查长度是否足够
        if ((nums.length == 0) || (len < n)) {
            return null;
        }
        if (n == 0) {
            ret.add(new ArrayList<>());
            return ret;
        }
        for (int i = start; i < nums.length; ++i) {
            List<List<Integer>> ss = _subsets(nums, i + 1, n - 1); // 凑剩下的n - 1个元
            if (ss != null) {
                if (ss.get(0).size() > 0) {
                    for (List<Integer> subset : ss) {
                        List<Integer> ns = new ArrayList<Integer>();
                        ns.add(nums[i]);
                        for (Integer si : subset) {
                            ns.add(si);
                        }
                        ret.add(ns);
                    }
                } else {
                    List<Integer> ns = new ArrayList<Integer>();
                    ns.add(nums[i]);
                    ret.add(ns);
                }
            }
        }
        return ret;
    }
}
```

## 递归的局限性，以及如何用循环替代递归。

递归调用有参数压栈的开销，包括执行效率和空间两方面。特别是空间的开销，在递归层次较深的情况下会导致堆栈溢出。针对这种情况一般做如下处理：

- 编译器进行尾递归优化
- 把递归写成循环。

尾递归有应用场景限制，只能用于f(n) -> f(n - 1)的场景，比如f(n) = q(f(n - 1))就不能应用了，因为每次递归还是需要堆栈保留递归状态。视场景不同，有两种套路：

- 简单迭代。自顶向下递归变成自底向上迭代。

```java
// 斐波那契数列f(n) = f(n - 1) + f(n - 2)
private int fib(int n) {
    if ((n == 0) || (n == 1)) {
        return 1;
    }
    int r = 0;
    int n0 = 1, n1 = 1;
    for (int i = 2; i <= n; ++i) {
        r = n0 + n1;
        n0 = n1;
        n1 = r;
    }
    return r;
}
```

- 手工维护堆栈进行递归状态的管理。

### [二叉树的前序遍历](https://www.lintcode.com/problem/binary-tree-preorder-traversal/)

前序遍历的顺序是 root->左子树->右子树，压栈时遵循先进后出的原则，先压右子树确保左子树先被处理。

实现代码如下：

```java
public class Solution {
    /**
     * @param root: A Tree
     * @return: Preorder in ArrayList which contains node values.
     */
    public List<Integer> preorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();
            stack.push(root);
            while (!stack.isEmpty()) {
                TreeNode node = stack.pop();
                ret.add(node.val);
                if (node.right != null) {
                    stack.push(node.right);
                }
                if (node.left != null) {
                    stack.push(node.left);
                }
            }
        }
        return ret;
    }
}
```

### [二叉树的中序遍历](https://www.lintcode.com/problem/binary-tree-inorder-traversal/)

前序遍历的顺序是 左子树->root->右子树，压栈处理方式要发生一些变化：栈的初始内容还是根节点。

- 从栈里弹出一个节点
- 先右子树入栈
- 左子树是否为空：
    - 是：输出节点
    - 否：复制根节点（左右节点置为null），入栈。然后左子树入栈。

实现代码如下：

```java
public class Solution {
    /**
     * @param root: A Tree
     * @return: Inorder in ArrayList which contains node values.
     */
    public List<Integer> inorderTraversal(TreeNode root) {
        // write your code here
        List<Integer> ret = new ArrayList<>();
        if (root != null) {
            Stack<TreeNode> stack = new Stack<>();
            stack.push(root);
            while (!stack.isEmpty()) {
                TreeNode node = stack.pop();
                if (node.right != null) {
                    stack.push(node.right);
                }
                if (node.left != null) {
                    TreeNode tmp = new TreeNode(node.val);
                    stack.push(tmp);
                    stack.push(node.left);
                } else {
                    ret.add(node.val);
                }
            }
        }
        return ret;
    }
}
```

## 课程推荐

[![大数据工程师集训营](../ad_img/bigdata.jpg)](http://www.julyedu.com/Weekend/bigdata)

课程链接：[http://www.julyedu.com/Weekend/bigdata](http://www.julyedu.com/Weekend/bigdata)

原价<b>9499</b>面向“Java与数据结构”班学员特别优惠价格<b>6499</b>!!!报名点击报名页面的“<b>课程咨询</b>”按钮信息修改价格。

### 课程简介

- 本大数据集训营从Hadoop基础讲解，贯穿数据采集、传输、存储、计算、展示等各个环节，着重讲解企业中如何使用spark、MapReduce、hive、flume、sqoop等各个组件，并附有经典企业案例讲解，案例均来自一线互联网工业项目。
- 2019年11月下旬时，我们讲师团队再次加强，堪称大厂豪华级大数据专家讲师团队，且根据最近的大数据人才需求，加入elasticsearch和数据仓库模型等内容。
- 2020年3月下旬时，全面优化大纲，比如新增以下三大实战项目，且标准化项目流程：
  - 设计与搭建基于Hive、Presto的数据仓库与OLAP分析引擎
  - Flink企业实战——直播、短视频APP用户行为分析
  - ELK+Spark实现一个错误日志监控(搜索、分析、报警)平台

### 培养目标

从<b>零</b>开始，由Hadoop入门，打造大数据开发工程师之路。