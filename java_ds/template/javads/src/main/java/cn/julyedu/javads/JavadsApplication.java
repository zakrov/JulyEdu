package cn.julyedu.javads;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavadsApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(JavadsApplication.class, args);
	}

    @Override
    public void run(String[] args) throws Exception {
        // 代码写在这里
        System.out.println("Hello world!");
    }
}

